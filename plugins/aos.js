import AOS from 'aos';
import 'aos/dist/aos.css';

// eslint-disable-next-line import/no-default-export
export default ({ app }) => {
  // eslint-disable-next-line no-param-reassign, new-cap
  app.AOS = new AOS.init({ disable: 'phone' }); // or any other options you need
};
