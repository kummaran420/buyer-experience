---
  title: GitLab vs Argo
  hero:
    title: GitLab vs Argo
    subtitle: How does GitLab compare to Argo in the Configure stage?
    icon:
      name: configure
      alt: configure Icon
      variant: marketing
      size: md
    crumbs:
      - title: DevOps maturity comparison
        href: /competition/
        data_ga_name: Competition
        data_ga_location: breadcrumb
      - title: GitLab vs. Argo
  overview:
    comparisons:
      - title: GitLab
        image: /nuxt-images/developer-tools/harvey-balls/50.svg
      - title: Argo CD
        image: /nuxt-images/developer-tools/harvey-balls/25.svg
    left_card:
      title:  Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes.
      description: |
        As far as completeness of capabilities in Configure, GitLab is ahead of Argo CD with respect to deployments to heterogeneous environments (i.e. K8s and non-K8s) and with respect to push-based (agentless or agent-based) and pull-based (agent-based) approaches to GitOps.

        In most of the categories that Argo CD offers, GitLab is fairly evenly matched for feature comparison, although for completeness of capabilities for Kubernetes-only deployments, GitLab lags behind Argo CD.
    right_card:
      title: GitLab's product roadmap
      bullets:
        - title: Move the Kubernetes Management category to Complete during Q4
        - title: Improving all existing features of the Infrastructure as Code category, especially various Terraform features
      button:
        text: GitLab releases
        link: /releases/
  analysis:
    side_by_side:
      stage: Configure
      icon:
        name: configure
        alt: configure Icon
        variant: marketing
        size: md
      tabs:
        - title: Auto DevOps
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Commit your code and GitLab does the rest to build, test, deploy, and monitor automatically. Eliminate the complexities of getting going with automated software delivery by automatically setting up the pipeline and necessary integrations, freeing up your teams to focus on the culture part.
              sections:
                - title: Details
                  content: |
                    * Auto DevOps is a market differentiator for GitLab
                    * A prescribed and customizable pipeline template based on best DevOps practices
                    * Its building blocks include templates for building, testing, deployment, etc., which can be reused and customized
                    * Their use increase productivity and speed time-to-market
                    * Automatic language recognition included as well as application of appropriate testing scanners
                    * Auto DevOps and Auto Deploy now works with the GitLab Agent for Kubernetes
                - title: Improving our product capabilities
                  content: |
                    * Auto DevOps direction [page](https://about.gitlab.com/direction/configure/auto_devops/#whats-next--why)
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/topics/autodevops/
            - title: Argo CD
              harveyball: /nuxt-images/developer-tools/harvey-balls/0.svg
              description: Argo CD does not offer this.
        - title: Kubernetes Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/25.svg
              description: Connect Kubernetes clusters to GitLab for deployments and insights.
              sections:
                - title: Details
                  content: |
                    * The certificate-based integration/connectivity to Kubernetes has been deprecated in favor of the GitLab Agent for Kubernetes
                    * You can securely connect to a cluster via CI/CD pipelines (GitLab CI/CD workflow, which uses the GitLab Agent for Kubernetes to connect to the K8s cluster). We call this push-based GitOps (our competitors position this as CIOps)
                    * You can securely connect to a cluster via the GitLab Agent for Kubernetes (GitOps workflow). We call this pull-based GitOps

                - title: Improving our product capabilities
                  content: |
                    * Kubernetes Dashboard [Epic](https://gitlab.com/groups/gitlab-org/-/epics/2493)
                    * The upcoming GitLab Observability UI (ex-Opstrace) will re-introduce metrics dashboards, including K8s-related ones
                    * Monitor:Observability [direction page](https://about.gitlab.com/direction/monitor/observability/#planning)
                    * It would be useful to [add some GitLab documentation](https://docs.gitlab.com/ee/user/clusters/agent/gitops.html#gitops-annotations) with an example on how to specify the synchronization order of K8s resources similar to the way [Argo CD does it](https://argo-cd.readthedocs.io/en/stable/user-guide/sync-waves/).

              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/project/clusters/
            - title: Argo CD
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: Argo CD is a GitOps CD tool for K8s. It manages the CD of applications deployed to K8s.
              sections:
                - title: Details
                  content: |
                    * Argo CD can connect to one of many K8s clusters with the goal of deploying and managing applications on them. But it does not manage the cluster themselves.
                    * Argo CD can sync resources in Git with K8s clusters.

              button:
                title: Documentation
                link: https://argo-cd.readthedocs.io/en/stable/#how-it-works
        - title: Deployment Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: "Enable platform engineers to use GitLab as their deployment platform: platform engineers can define common DevOps practices, streamline compliance, and share common patterns to enable application development teams to be more efficient."
              sections:
                - title: Details
                  content: |
                    * GitLab has Progressive Delivery capabilities, e.g. [feature flags](https://docs.gitlab.com/ee/topics/release_your_application.html#feature-flags).
                    * GitLab has [Auto DevOps and Auto Deploy](https://docs.gitlab.com/ee/topics/release_your_application.html#deploy-with-auto-devops) reusable templates
                    * GitLab Auto Deploy has [built-in support](https://docs.gitlab.com/ee/topics/release_your_application.html#deploy-to-aws-with-gitlab-cicd) for Amazon EC2 and ECS deployments, in addition to Kubernetes
                    * GitLab supports [push-based GitOps via the CI/CD workflow](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html) for the deployment of applications
                    * GitLab supports [pull-based GitOps via the GitLab Agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/gitops.html)
                - title: Improving our product capabilities
                  content: |
                    * Show synced status of manifest projects [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/258603)
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/topics/release_your_application.html
            - title: Argo CD
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes. Argo Workflows is their CI/CD container-native solution.
              sections:
                - title: Details
                  content: |
                    * Argo CD was built for Kubernetes and can connect and deploy applications to one or many K8s clusters.
                    * Argo CD can connect and deploy applications to one or many clusters via [its declarative setup](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#applications).
                    * Argo CD only supports the pull-approach to CD, which many say it’s the only way to do GitOps.
                    * Argo CD provides a very interactive browser-based user interface with views for application topologies, Git-K8s synching information, event logs, etc.
                    * Argo CD [architectural overview](https://argo-cd.readthedocs.io/en/stable/operator-manual/architecture/) showing it can deploy applications to many K8s clusters.
                    * Argo CD supports [automation from CI pipelines](https://argo-cd.readthedocs.io/en/stable/user-guide/ci_automation/), but this is just a webhook to have ArgoCD run a synchronization using their pull-based approach.
                    * Argo Workflows is their [CI/CD solution for K8s](https://argoproj.github.io/argo-workflows/) and as such, it is capable of supporting push-based GitOps.
              button:
                title: Documentation
                link: https://argo-cd.readthedocs.io/en/stable/
        - title: ChatOps
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Tight integrations with Slack and Mattermost make it easy to manage and automate software development and delivery right from your chat app.
              sections:
                - title: Details
                  content: |
                    * GitLab supports ChatOps via [slash commands](https://docs.gitlab.com/ee/integration/slash_commands.html) that can be used from Slack and Mattermost.
                    * For notifications, GitLab also integrates with other chat solutions, such as MS Teams, Discord, Google Chat, Pumble, Circuit, WebEx Teams, and webhooks for custom integrations using HTTP requests.

                - title: Improving our product capabilities
                  content: |
                    * No short-term roadmap plans. But long-term plan is to support integration with tools such as Mattermost or Microsoft Teams.
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/ci/chatops/
            - title: Argo CD
              harveyball: /nuxt-images/developer-tools/harvey-balls/0.svg
              description: Argo CD does not offer this.
              sections:
                - title: Details
                  content: |
                    * Argo CD can integrate with some chat solutions for notifications but it does not provide any type of slash commands to do ChatOps.
                    * For notifications, Argo CD integrates with Slack, Google Chat, Mattermost, MS Teams, Rocket.Chat, Telegram, and webhooks for custom integrations using HTTP requests.
              button:
                title: Documentation
                link: https://argo-cd.readthedocs.io/en/stable/operator-manual/notifications/
        - title: Infrastructure as Code (IaC)
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description:  Manage your infrastructure effectively to create, configure, and manage a complete software development environment.
              sections:
                - title: Details
                  content: |
                    * GitLab has tight integration with Terraform
                      * [GitLab Terraform provider](https://github.com/gitlabhq/terraform-provider-gitlab)
                      * [GitLab-managed Terraform state file](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
                      * For collaborating around Infrastructure as Code (IaC) changes, GitLab provides [Terraform integration in merge requests](https://docs.gitlab.com/ee/user/infrastructure/iac/mr_integration.html)
                      * GitLab-authored Terraform reusable [templates](https://docs.gitlab.com/ee/user/infrastructure/iac/index.html#latest-terraform-template)
                      * [Built-in Terraform module registry](https://docs.gitlab.com/ee/user/packages/terraform_module_registry/index.html)
                      * [Terraform SAST scanners](https://docs.gitlab.com/ee/user/application_security/iac_scanning/index.html#configure-iac-scanning-manually)
                      * In addition, infrastructure components, such as an Nginx server, can be expressed in manifest files (IaC) in GitLab and their deployments can be managed by GitLab.

                - title: Improving our product capabilities
                  content: |
                    * Infrastructure-as-Code direction [page](https://about.gitlab.com/direction/configure/infrastructure_as_code/#today)
                    * [Interaction with Policy as Code (OPA)](https://about.gitlab.com/direction/configure/infrastructure_as_code/#interaction-with-policy-as-code)
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/infrastructure/iac/index.html
            - title: Argo CD
              harveyball: /nuxt-images/developer-tools/harvey-balls/25.svg
              description: Argo CD supports the pull-based GitOps for Kubernetes IaC only.
              sections:
                - title: Details
                  content: |
                    * Argo CD is focused on application deployments using manifest files in YAML/JSON/Jsonnet, Kustomize, and Helm. Infrastructure components, such as an Nginx server, can be expressed in manifest files (IaC) and their deployments managed by Argo CD. This is why Argo CD gets a ¼ Harvey ball for this category.
                    * If we limit this category to just Terraform, then Argo CD would get a blank Harvey ball.
        - title: Cluster Cost Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/0.svg
              description: Gain insights and recommendations about your cluster spending
              sections:
                - title: Details
                  content: |
                    * This capability is currently [deprecated](https://docs.gitlab.com/ee/user/clusters/cost_management.html). Once the GitLab Agent for Kubernetes is more mature, [reopening the discussion](https://gitlab.com/gitlab-org/configure/general/-/issues/79#note_493996705) around what Cluster Cost Management tools might provide a lot of value to our users will likely be valid.
                - title: Improving our product capabilities
                  content: |
                    * Aside from the above kubecost work, [we are not actively prioritizing development work in this category](https://about.gitlab.com/direction/configure/cluster_cost_management/). We would welcome your contribution in this space.
                    * Top vision item: [Flag over-provisioned kubernetes deployments](https://gitlab.com/gitlab-org/gitlab-ee/issues/9049)

              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/clusters/cost_management.html
            - title: Argo CD
              harveyball: /nuxt-images/developer-tools/harvey-balls/0.svg
              description: Argo CD does not offer this.
              sections:
                - title: Details
                  content: |
                    * Argo CD does not include any capabilities to measure and/or manage cluster costs.
                    * Kubecost [can be installed](https://blog.kubecost.com/blog/measuring-argo-workflows-with-kubecost/) with Argo CD to measure its cloud consumption costs.
                    * Kubecost is a separate open source project outside Argo CD.
              button:
                title: Documentation
                link: https://guide.kubecost.com/hc/en-us/articles/4407595950359

